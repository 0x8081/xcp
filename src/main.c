#include <stdio.h>

#include <xcp.h>
#include <svod.h>
#include <stfs.h>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("xcp [input file]\n");
        printf("xcp [input file] [key]\n");
        return -1;
    }

    XCP_CTX *ctx = xcp_init(argv[1]);
    if (ctx == NULL)
    {
        fprintf(stderr, "Failed to init XCP context, terminating...\n");
        return -1;
    }

    switch (ctx->type)
    {
    case XCP_SVOD:
        xcp_read_header(ctx);
        xcp_print_header(ctx);
        svod_decompress(ctx);
        break;

    case XCP_STFS:
        if (argc < 3)
        {
            fprintf(stderr, "Please provide key\n");
            break;
        }
        stfs_decrypt(ctx, argv[2]);
        break;

    case XCP_INVALID:
        fprintf(stderr, "Invalid XCP!\n");
        break;
    }

    xcp_free(ctx);
}