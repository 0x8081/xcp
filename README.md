# XCP

A simple XCP decompressor written in C++.

### Prerequisites

You need the following libraries installed:
  * zlib
  * mspack

### Building

The Makefile uses gcc by default. You can use gcc, g++, or clang to compile.

Compile:
  ```
  make
  ```

### Usage

Decompress an SVOD XCP:
  ```
  ./xcp <FILE>
  ```

Decrypt and Decompress an STFS XCP:
  ```
  ./xcp <FILE>
  ```