TARGET     = xcp
CC         = gcc
SRCDIR     = src
CFLAGS     = -Iexternal/libxcp/src
LDFLAGS    = -Lexternal/libxcp -lz -lcrypto -lxcp -lmspack
SRCS       = $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.c))
OBJS       = $(SRCS:.c=.o)

release: $(TARGET)

$(TARGET): $(OBJS)
	$(MAKE) -C external/libxcp libxcp.a
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@

clean:
	$(MAKE) -C external/libxcp clean
	rm $(TARGET) $(OBJS)
